package pos.theorie.epic;

import java.io.*;

public class DateiEinlesen {

    private static final String FILENAME = "src/pos/theorie/epic/input.txt";

    public static void main(String[] args) {

        try {

            File file = new File(FILENAME);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            // BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(FILENAME)));

            String line = bufferedReader.readLine();
            while(line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }

            bufferedReader.close();
            fileReader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

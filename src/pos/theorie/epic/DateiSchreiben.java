package pos.theorie.epic;

import java.io.*;

public class DateiSchreiben {

    private static final String FILENAME = "src/pos/theorie/epic/output.txt";

    public static void main(String[] args) {

        try {

            File file = new File(FILENAME);
            FileWriter fileWriter = new FileWriter(file, false); // append true/false
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            // BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(FILENAME)));

            bufferedWriter.write("Inhalt Zeile 1\n");
            bufferedWriter.write("Inhalt Zeile 2\n");
            bufferedWriter.write("Inhalt Zeile 3");

            bufferedWriter.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

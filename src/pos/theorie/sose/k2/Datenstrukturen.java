package pos.theorie.sose.k2;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Datenstrukturen {

    public static final char WASSER = 'W';

    public static void main(String[] args) {

        int[] zahlen = new int[20];

        zahlen[9] = 4;
        System.out.println(zahlen[3]);

        for (int z : zahlen) {
            System.out.println(z);
        }

        for (int i = 0; i < zahlen.length; i++) {
            int z = zahlen[i];
            System.out.println(z);
        }

        String[] texte = {"A", "B", "C"};


        // mehrdimensionale Array
        double[][] tabelle = new double[4][10];
        tabelle[1][8] = 4.0;

        for (int i = 0; i < tabelle.length; i++) {
            for (int j = 0; j < tabelle[i].length; j++) {
                tabelle[i][j] = 1;
                System.out.println(tabelle[i][j]);
            }

        }


        ArrayList<Integer> zahlen2 = new ArrayList<>();
        zahlen2.add(4);
        zahlen2.add(6);
        zahlen2.add(2);
        zahlen2.add(1,20);
        zahlen2.set(0, 5);

        for (Integer z : zahlen2) {
            System.out.println(z);
        }


        char[][] spielfeld = new char[10][5];
        spielfeld[0][0] = WASSER;


        // array + schleife
        long[] grosseZahlen = new long[100];
        grosseZahlen[0] = 1;

        for(int i = 1; i < grosseZahlen.length; i++) {
            grosseZahlen[i] = grosseZahlen[ i - 1 ] * 2;
        }

        for (long z : grosseZahlen) {
            System.out.print(z + " ");
        }


    }

}

package pos.theorie.sose.k2;

import java.util.Random;

public class Zeitmessung {

    private int size;
    private int[] array;

    public Zeitmessung(int size) {
        this.size = size;
        array = new int[size];
    }

    public void initialize() {
        Random random = new Random();

        for(int i = 0; i < size; i++) {
            array[i] = random.nextInt();
        }
    }

    public void measurement(int index) {
        long start = System.nanoTime();

        for (int i = 0; i < 1000; i++) {
            int tmp = array[index];
        }

        long stop = System.nanoTime();

        long diff = (stop - start) / 1000;

        System.out.println("array " + index + ": " + diff + "ns");
    }

    public static void main(String[] args) {

        Zeitmessung zeitmessung = new Zeitmessung(1000);
        zeitmessung.initialize();
        zeitmessung.measurement(0);
        zeitmessung.measurement(500);
        zeitmessung.measurement(999);

        zeitmessung = new Zeitmessung(10000);
        zeitmessung.initialize();
        zeitmessung.measurement(0);
        zeitmessung.measurement(5000);
        zeitmessung.measurement(9999);

        zeitmessung = new Zeitmessung(100000);
        zeitmessung.initialize();
        zeitmessung.measurement(0);
        zeitmessung.measurement(50000);
        zeitmessung.measurement(99999);

    }

}

package pos.theorie.sose.k3;

import java.util.Scanner;

public class ThreeDArray {

    public static void main(String[] args) {

        int s = 1000;
        long c = 0;

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        int[][][] a = new int[s][s][s]; // 1000 x 1000 x 1000 = 1.000.000.000 = 1 * 10^9 * 4 Byte

        for (int x = 0; x < s; x++) {
            for (int y = 0; y < s; y++) {
                for (int z = 0; z < s; z++) {
                    a[x][y][z] = 1;
                    c++;
                }
            }
        }

        System.out.println(c);

        Scanner scan = new Scanner(System.in);

        String s1 = "ABCD";

        String s2 = scan.next();

        System.out.println(s1 == s2);
        System.out.println(s1.equals(s2));


    }
}

package pos.theorie.sose.k6;

import java.util.ArrayList;
import java.util.List;

public class Bruch implements Berechnung, Grundrechenarten {
    private int zaehler;
    private int nenner = 1;

    public static final Bruch NULL = new Bruch(0,1);
    public static final Bruch EINS = new Bruch(1,1);
    public static final Bruch HALBE = new Bruch(1,2);

    private static int anzahlBrueche = 0;

    private static void zaehleBruche() {
        anzahlBrueche++;
    }

    public Bruch() {
        zaehleBruche();
    }

    public Bruch(int z) {
        zaehleBruche();
    }

    public Bruch(int z, int n) {
        zaehleBruche();
    }

    public int getNenner() {
        return nenner;
    }

    public void setNenner(int nenner) {
        if (nenner >= 0) {
            this.nenner = nenner;
        } else {
            System.out.println("Fehler");
        }
    }

    @Override
    public Bruch addieren(Bruch b) {
        Bruch ergebnis = new Bruch();
        ergebnis.zaehler = this.zaehler + b.zaehler;    // demo
        return ergebnis;
    }

    @Override
    public double berechne() {
        return 0;
    }

    public static void main(String[] args) {
        Bruch a = new Bruch();
        Bruch b = new Bruch(3);
        Bruch c = new Bruch(1, 2);
        a.addieren(b);
        b.addieren(c);

        double d = Double.parseDouble("2.3");

        Bruch.anzahlBrueche = 0;
        Bruch.zaehleBruche();

        b.zaehler = 5;

        List<Integer> list = new ArrayList<Integer>();

    }

}

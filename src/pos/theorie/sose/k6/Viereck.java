package pos.theorie.sose.k6;

public class Viereck {

    public final static int SEITENANZAHL = 4;

    private static int anzahl = 0;

    public Viereck() {
        anzahl++;
    }

    private double laenge;

    public void setLaenge(double laenge) {
        if (laenge >= 0) {
            this.laenge = laenge;
        } else {
            System.out.println("Fehler");
        }
    }


    public void berechneFlaeche() {
        System.out.println("berechneFlaeche Viereck");
    }

    public static void main(String[] args) {

        double x = Double.parseDouble("1.54");

        new Viereck();
        new Viereck();
        new Viereck();
        new Viereck();

        Viereck v1 = new Viereck();
        v1.laenge = -10;
        v1.setLaenge(-10);

        Viereck v2 = new Viereck();
        v2.laenge = 20;

        Viereck v3 = new Viereck();
        v3.laenge = 30;
        System.out.println(Viereck.anzahl);

    }

}

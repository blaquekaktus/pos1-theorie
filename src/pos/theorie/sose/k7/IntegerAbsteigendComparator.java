package pos.theorie.sose.k7;

import java.util.Comparator;

/**
 * Comparter zum absteigenden Sortieren von Zahlen
 */
public class IntegerAbsteigendComparator implements Comparator<Integer> {
    @Override
    public int compare(Integer o1, Integer o2) {
        return o2.compareTo(o1);
    }
}

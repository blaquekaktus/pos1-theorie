package pos.theorie.wise.k3;

import java.util.Scanner;

public class Aussagenlogik {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("A:\t");
        boolean a = scan.nextBoolean();

        System.out.print("B:\t");
        boolean b = scan.nextBoolean();

        boolean aUndB = a && b;
        boolean aOderB = a || b;
        boolean nichtA = !a;

        System.out.println("A&&B:\t" + aUndB);
        System.out.println("A||B:\t" + aOderB);
        System.out.println("!A:\t" + nichtA);

        System.out.println("xy:\t" + (!a && (b | !b)));

        if (a && !b) {
            System.out.println("XY");
        }
    }
}

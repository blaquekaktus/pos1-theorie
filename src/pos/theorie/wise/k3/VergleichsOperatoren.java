package pos.theorie.wise.k3;

import java.util.Scanner;

public class VergleichsOperatoren {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.print("Bitte eine Zahl eingeben: ");
        double eingabe = scan.nextDouble();

        if (eingabe > 50) {
            System.out.println("zu groß");
        } else {
            System.out.println("ok");
        }
    }
}

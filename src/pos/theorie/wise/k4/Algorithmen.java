package pos.theorie.wise.k4;

import java.util.Scanner;

public class Algorithmen {

    public static void main(String[] args) {


        // 15.10.2020 =========================================

        for (int i=0; i<10; i++) {
            System.out.println(i);
        }

        int i=1;
        while (i<10) {
            System.out.println(i*i);
            i++;
        }


        // 22.10.2020 =========================================

        // Modulo Operator -> Rest berechnen
        int rest = 17 % 3;  // Rest = 2

        // Eingabe mit Scanner
        Scanner scan = new Scanner(System.in);

        // Eingabe einer Zahl
        System.out.print("Bitte Zahl eingeben: ");
        int eingabe = scan.nextInt();

        // oder Eingabe von Text
        System.out.print("Bitte Zahl eingeben: ");
        String s = scan.next(); // liest bis zum nächsten Leerzeichen (Token)
        s = scan.nextLine();    // liest bis zum Zeilenumbruch (Token)

        // Umwandeln von String nach Int
        int e = Integer.parseInt(s);

        scan.close();   // Scanner (Ressourcen) immer am Ende schließen

        String binaer = "001010111";

        System.out.println("Zeichen an Stelle 0: " + binaer.charAt(0));    // erste Stelle = 0
        System.out.println("Zeichen an Stelle 8: " + binaer.charAt(8));    // letzte Stelle = 1
        System.out.println("Zeichen an Stelle 8: " + binaer.charAt(binaer.length()-1)); // letzte Stelle

        char c = binaer.charAt(8);

        // Achtung: der char '0' ist nicht das selbe wie der int 0
        // Konvertierung erfolgt anhand der ASCII Tabelle
        // https://de.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange

        // ASCII-Wert char '0' ist int 48
        if (c == '0') { // alternativ:  if (c == 48)
            System.out.println("char c ist 0");
        }

        // ASCII-Wert char '1' ist int 49
        if (c == '1') {
            System.out.println("char c ist 1");
        }

    }
}

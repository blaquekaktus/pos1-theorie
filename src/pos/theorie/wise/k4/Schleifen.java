package pos.theorie.wise.k4;

import java.util.Random;

public class Schleifen {

    public static void main(String[] args) {

        int x = 0;

        while(x < 10) {
            System.out.println("wahr x=" + x);

            x++;
        }

        System.out.println("ende");
        System.out.println(x);




        for(int i = 0; i > -10; i--) {
            System.out.println("wahr i=" + i);
        }

        System.out.println("ende");



        do {
            System.out.println("do - while");
        }while (x < 10);

        String z1 = "1010110";

        System.out.println(z1.charAt(0));



        long z2 = 110;

        System.out.println(z2 / 10);    // 11 ergebnis
        System.out.println(z2 % 10);    // 0 rest c0

        z2 = z2 / 10;   // 11

        System.out.println(z2 / 10);    // 1 ergebnis
        System.out.println(z2 % 10);    // 1 rest c1

        z2 = z2 / 10;   // 1

        System.out.println(z2 / 10);    // 0 ergebnis
        System.out.println(z2 % 10);    // 1 rest c2




    }
}

package pos.theorie.wise.k5;

import java.math.BigInteger;

public class Fakultaet {

    public static void main(String[] args) {


        BigInteger a = new BigInteger("4");
        BigInteger b = new BigInteger("3");  // BigInteger.TWO;
        BigInteger c = a.add(b); //a + b;

        System.out.println(c);



        System.out.println(c.compareTo(new BigInteger("5")));

        // if ( c > 5 )     c.compareTo(new BigInteger("5")) == 1
        // if ( c >= 5 )    c.compareTo(new BigInteger("5")) >= 0
        // if ( c < 5 )     c.compareTo(new BigInteger("5")) == -1
        // if ( c == 5 )    c.compareTo(new BigInteger("5")) == 0

        // if (c > 5) {
        if (c.compareTo(new BigInteger("5")) == 1) {
            System.out.println("größer 5");
        } else {
            System.out.println("nicht größer 5");
        }



    }


}

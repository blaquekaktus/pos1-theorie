package pos.theorie.wise.k5;

public class Funktionen {

    public static void main(String[] args) {

        String s = "Hallo Welt";

        // Instanzmethoden
        System.out.println(s.length());
        System.out.println(s.charAt(0));
        System.out.println(s.substring(4));

        // Klassenmethoden
        System.out.println(Math.floor(54.3));
        System.out.println(Math.pow(2, 5));


        Rechner r = new Rechner();

        // Methode wird zur Laufzeit abhängig von den Parameterwerten ausgewählt
        // call by value -> primitive Parametertypen
        r.addition(5, 3);    // int, int
        r.addition(4.5, 3);  // double, int
        //r.addition(4.5, 4.4);   // double, double

        Box a = new Box(5);
        Box b = new Box(3);

        System.out.println("a: " + a.value);    // a.value = 5

        // call by reference -> Referenz-Parametertypen
        int e = r.addition(a, b);               // Wert von a wird in der Methode auf 0 gesetzt
        System.out.println("Ergebnis " + e);

        System.out.println("a: " + a.value);    // a.value = 0
        System.out.println("b: " + b.value);

    }

}

package pos.theorie.wise.k5;

public class Rekursion
{

    public static void main(String[] args) {

        long start = System.currentTimeMillis();

        recursion(15000);

        long stop = System.currentTimeMillis();

        long diff = stop - start;

    }


    public static void recursion(int count) {

        if (count > 0) {

            System.out.println("recursion: " + count);

            recursion(count - 1);
        }

    }




}
